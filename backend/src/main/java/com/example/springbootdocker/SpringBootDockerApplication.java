package com.example.springbootdocker;

import org.springframework.web.bind.annotation.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

@SpringBootApplication
@RestController
public class SpringBootDockerApplication {

	static Logger logger = LogManager.getLogger(SpringBootDockerApplication.class);

	@GetMapping("/api/profile")
	@ResponseBody
	public String getProfile(@RequestParam String id) {
		logger.info("Requested profile ID:{}", id);
		return "ID: " + id;
	}

	@GetMapping("/messages")
    public String getMessage() {
        return "Getting messages";
    }

	@PostMapping("/api/rewards/claim")
	public String claimRewards(@RequestParam int points) {
		// TODO: Not fully implemented yet. But eventually it will fetch and update the balance from DB
		int balance = 100;
		balance -= points;

		return "New Balance: " + balance;
	}

	@RequestMapping("/")
	public String home() {
		return "Welcome to Docker World";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerApplication.class, args);
	}

}
