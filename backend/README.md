## Introduction
This project is safe to run locally.

## Dev guide
To build the project, run this command at backend's root folder:
```shell
$ ./gradlew clean build
```
Build the image
```
$ docker build -t xyz/backend:5.0 .
```
View the image
```shell
$ docker images                    
REPOSITORY       TAG       IMAGE ID       CREATED              SIZE
xyz/backend      5.0       5a526e120c1d   About a minute ago   459MB

```
Start the container
```shell
docker run -p 9091:9091 -t xyz/backend:5.0
```

Go to `https://localhost:9091`

## Database
If you need the database password / credentials, please search in our Confluence page.