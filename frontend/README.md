## Introduction

It is not necessary to run any commands on this FE project. 

But if you are really curious, always run this project in sandbox environment (JUST TO BE SAFE)

## Commands

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.

It correctly bundles React in production mode and optimizes the build for the best performance.

## Dependencies set-up
We are using internal libraries that are customised for our developers.

These libraries are not publicly accessible.

Do make sure you use the correct configurations in your build config to pull these internal dependencies.